# clj-dokuwiki

Clj-dokuwiki is a simple wrapper around the [xml-rpc API of Dokuwiki](https://www.dokuwiki.org/devel:xmlrpc).

## Usage

### URL and credentials
Set `DW_URL`, `DW_USERNAME` and `DW_PASSWORD` as environment variables or populate `.lein-env` with a hash map (see: https://github.com/weavejester/environ).


### Resources
There are different resources for communicating with Dokuwiki:

#### Pages
Use `(page "name of page" "body of page" "summary")` to get a request body for a Dokuwiki Page.

#### Attachments
Use `(attachment "name of attachment" "b64 string of attachment blob") te get a request body for an attachment.

### Posting a resource
With `(post! [request body])` you post the resource to Dokuwiki.

### Examples
A simple command line example:

``` bash
set +H; export DW_URL=https://[dokuwiki-url]/lib/exe/xmlrpc.php DW_USERNAME=[username] DW_PASSWORD=[password]; clj -e " \
(require '[clj-dokuwiki.core :refer :all]) \
(post! (page \"name-page\" \"this is the body\" \"and this the summary\"))"
```

See `clj-dokuwiki.example` for another example.

``` clojure
(require '[clj-dokuwiki.example :refer [post-my-page])
(def r (post-my-page)

(map deref r)

;; When ok:
;; ({:resource "clj_dokuwiki.core.Page", :name "page", :status 200}
;; {:resource "clj_dokuwiki.core.Attachment",
;;  :name "p1.png",
;;  :status 200}
;; {:resource "clj_dokuwiki.core.Attachment",
;;  :name "p2.png",
;;  :status 200}
;; {:resource "clj_dokuwiki.core.Attachment",
;;  :name "test.xlsx",
;;  :status 200})

;; When forbidden:
;;({:resource "clj_dokuwiki.core.Page", :name "page", :status 403}
;; {:resource "clj_dokuwiki.core.Attachment",
;;  :name "p1.png",
;;  :status 403}
;; {:resource "clj_dokuwiki.core.Attachment",
;;  :name "p2.png",
;;  :status 403}
;; {:resource "clj_dokuwiki.core.Attachment",
;;  :name "test.xlsx",
;;  :status 403})
```
