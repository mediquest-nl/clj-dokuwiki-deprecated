(ns clj-dokuwiki.example
  (:require [clj-dokuwiki.core :refer [post! page attachment]]
            [environ.core :refer :all])
  (:import java.util.Base64))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Helper functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn slurp-bytes
  "Slurp the bytes from a slurpable thing"
  [x]
  (with-open [out (java.io.ByteArrayOutputStream.)]
    (clojure.java.io/copy (clojure.java.io/input-stream x) out)
    (.toByteArray out)))

(defn encode [to-encode]
  (.encodeToString (Base64/getEncoder) to-encode))

(defn body-str [& strings] (clojure.string/join "\n" strings))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; End of helper functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn post-my-page
  "Post asynchronous a page and 3 attachments.
  Collect these resources in a vector and map the function `post*` over these
  resources. The `doall` is necessary to force the side effect (i.e. posting
  of the resource).
  Set `DW_URL`, `DW_USERNAME` and `DW_PASSWORD` as environment variables or
  populate a file `.lein-env` with a hash map
  (see: https://github.com/weavejester/environ)"
  []
  (let [{:keys [dw-url dw-username dw-password]} env
        body (body-str
              "====== Een pagina ======"
              "Dit is een pagina zonder enige bijzonderheden"
              "===== Overzichten ====="
              "  * {{ test.xlsx |}}"
              "{{ p1.png |}}"
              "{{ p2.png |}}"
              )
        attachment1 (-> "resources/test.xlsx"
                        slurp-bytes
                        encode)
        attachment2 "iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAMAAAC6V+0/AAAA1VBMVEUAAAD///9mPRZUKwL/3E//8pH/4E//4k//x1b/+ub/7oj/w3b/0n3/23r/zHr/+u7/s0SyiTT/rTv/5mr/uz///dn/3GL/y2P/8uL/pj3/68//z2//uWj/slj+rE3/uU3/myn/1U63t7f/2pn/1Vr/xV7/753/+J3/9uz/8Nr/wVXm1bX/7X722J6ygS7nmjW2jkPm3Mf/wGHDw8P//c3/0ZT/rS//7ML//b//56L/9LT//a6+rIi8rpXxzYS2lljCkC3Jn0vBkTTJkR//4rD/1KH/x4UXpuqlAAABIUlEQVQYlUWQ6XaCMBBGk0jTaDCAQJFFZFNxX0Dcl1bb93+kJtHa++M7Z+7JzJkMAJKxSojjT5fgn7FzH+Z53vZS9aXPaTvXBHk+TO8vp9UfaFo7nT7GeXUNQs22RdY9OYF8ezaENViWIm1v7fCHbL0pYa0Gt1uR5WbNlkD9Ybu9KA8Hkfsdoykg1G/2eb89GIjsN33qAxL0MrPx9qRhZr3AAaQzQgo2Jw3OxMQKGnW4DLsoxvhdgnGMulyqoT4zFEVarCjGTA9TMNZdC8WxIoljZLldvj2JKgsZ2QcnM5BVuY74ph5VZIEkC1JFujzUuRWtfL/J8dkqak2fZ2q5lKkcRsM/lyTH6zwMKA3C+dcpSR62KJLj5+12vV5OSVFw8Qt/ISBONNUymQAAAABJRU5ErkJggg=="
        uploads [(page "page" body "summary")
                 (attachment "p1.png" attachment1)
                 (attachment "p2.png" attachment1)
                 (attachment "test.xlsx" attachment2)]]
    (doall (map post! uploads))))

;; (def result (post-my-page))
;; (map deref result)
