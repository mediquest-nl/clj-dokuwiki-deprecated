(ns clj-dokuwiki.core
  (:require [clj-http.client :as client]
            [clojure.data.xml :as xml]
            [environ.core :refer [env]]
            [clojure.spec.alpha :as s]))


(defprotocol Rpc
  (request [this]))

(defrecord Page [name body summary]
  Rpc
  (request [this]
    (let [xml (xml/sexp-as-element
               [:methodCall
                [:methodName "wiki.putPage"]
                [:params
                 [:param
                  [:value
                   [:string (:name this)]]]
                 [:param
                  [:value
                   [:string (:body this)]]]
                 [:param
                  [:value
                   [:struct
                    [:member
                     [:name "sum"]
                     [:value
                      [:string (:summary this)]]]
                    [:member
                     [:name "minor"]
                     [:value
                      [:boolean "True"]]]
                    ]]]]])]
      (xml/emit-str xml))))

(defn page
  "Returns a record of type `Page`"
  [name body summary]
  (->Page name body summary))

(defrecord Attachment [name body]
  Rpc
  (request [this]
    (let [xml (xml/sexp-as-element
               [:methodCall
                [:methodName "wiki.putAttachment"]
                [:params
                 [:param
                  [:value
                   [:string (:name this)]]]
                 [:param
                  [:value
                   [:base64 (:body this)]]]
                 [:param
                  [:value
                   [:struct
                    [:member
                     [:name "ow"]
                     [:value
                      [:boolean "True"]]]
                    ]]]]])]
      (xml/emit-str xml))))

(defn attachment
  "Returns a record of type `Attachment`"
  [name body]
  (->Attachment name body))

(def url-regex #"https?://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")

(s/def ::url-credentials (s/cat :dw-url (s/and string? #(re-matches url-regex %))
                          :dw-username string? :dw-password string?))

;; TODO: when url doesnt point to a valid Dokuwiki, it might return a status code
;; 200 / 301. It should be better if it raises an exception.

(defn post!
  "Posts a resource asynchronously to Dokuwiki.
  Takes a record which implements the protocol `Rpc` as `body`.
  Returns, when successful, a promise with a map with the name of the
  resource and page and a status code or else an exception message.

  Takes `DW_URL` as url  and `DW_USERNAME` and `DW_PASSWORD` as credentials
  when set as environment variables. If these are not set, you have to add them
  as parameters."
  ([url username password body]
   (let [p (promise)]
     (client/post url
                  {:async? true
                   ;; :oncancel #(println "request was cancelled")
                   :content-type "application/xml"
                   :redirect-strategy :lax
                   :insecure? true
                   :body (request body)
                   :basic-auth [username password]}
                  (fn [r] (deliver p {:resource (pr-str (type body))
                                      :name (:name body)
                                      :status (:status r)}))
                  (fn [exception] (deliver p {:resource (pr-str (type body))
                                              :name (:name body)
                                              :status (:status (ex-data exception))})))
     p))
  ([body]
   (let [{:keys [dw-url dw-username dw-password]} env
         input [dw-url dw-username dw-password]
         parsed (s/conform ::url-credentials input)]
     (if (= parsed ::s/invalid)
       (throw (ex-info "Invalid input" (s/explain-data ::url-credentials input)))
       (post! dw-url dw-username dw-password body)))))
